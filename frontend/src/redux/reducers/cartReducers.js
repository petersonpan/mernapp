import * as actionTypes from '../constants/Cartconstants';

const CART_INITIAL_STATE = {
    cartItems : [],
}

export const cartReducer = (state=CART_INITIAL_STATE,action) =>{
    switch(action.Type){
        case actionTypes.ADD_TO_CART:
           const item=action.payload;
           const exisitem = state.cartItems.find((x) => x.products === item.products)
            if(exisitem){
                return {
                    ...state,
                    cartItems: state.cartItems.map((x)=>x.products===exisitem.products ? item : x)
                };           
            }else{
                return {
                    ...state,
                    cartItems:[...state.cartItems,item],
                };
            }
        case actionTypes.REMOVE_TO_CART:
            return {
                ...state,
                cartItems:state.cartItems.filter((x) => x.products !== action.payload),
            }    
        default:
           return state;
    }
};