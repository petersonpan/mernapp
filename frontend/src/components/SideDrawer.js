import "./SideDrawer.css";
import {Link} from 'react-router-dom';
const SideDrawer = ({show,click}) =>{
    const sideDrawerClass = ["SideDrawer"];

    if(show){
        sideDrawerClass.push("show");
    }
    console.log("Result Show:",show);
    return (
        <div className={sideDrawerClass.join(" ")}>
            <ul className="SideDrawer__link" onClick={click}>
                <li>
                    <Link to="/cart">
                        <i className="fas fa-shopping-cart"></i>
                        <span>Cart<span className="SideDrawer__cartbadge">0</span></span>
                    </Link>
                </li>
                <li>
                    <Link to="/">Shop
                    </Link>
                </li>
            </ul>
        </div>
    );
};

export default SideDrawer;