import "./BackDrop.css";
//@params destructure show
const BackDrop = ({ click, show }) =>{
    console.log("Backdrop show result:",show);
    return show && <div className="BackDrop" onClick={click}></div>;
};

export default BackDrop;