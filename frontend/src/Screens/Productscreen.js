import './Productscreen.css';
import {useState,useEffect} from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { useParams,useNavigate } from 'react-router-dom';
import { getProductDetails } from '../redux/actions/Productaction';
import { addToCart } from '../redux/actions/Cartaction';

const Productscreen = ({match}) => {

    const [qty,setqty] = useState(1);
    const dispatch = useDispatch();
    const {id} = useParams();
    const history = useNavigate();
    const productDetails = useSelector(state => state.getProductDetails);

    const {product,loading,error} = productDetails;
    

    useEffect(() => {
        if (product && id !== product._id) {
          dispatch(getProductDetails(id));
        }
      }, [dispatch, product,id]);

      const addtoHandler = () => {
          dispatch(addToCart(product._id,qty));
          history(`/cart`);
      };

      return (
        <div className="productscreen">
            {
                loading ? ( <h2>loading...</h2>
                ) : error ? (  <h2>{ error }</h2> 
                ) : (
            <>        
            <div className="productscreen__left">
                <div className="left__image">
                    <img src={product.imageURL} alt={product.name}/>
                </div>
                <div className="left__info">
                    <p className="left__name">{product.name}</p>
                    <p>Price: ${product.price}</p>
                    <p>Description : {product.description}</p>
                </div>
            </div>
            <div className="productscreen__right">
                <div className="right__info">
                    <p>
                        Price:<span>${product.price}</span>
                    </p>
                    <p>Status:<span>{product.countInStock > 0 ?"In Stock":"Out of Stock"}</span>
                    </p>
                    <p>Qty
                        {product.countInStock}
                        <select name="qty" value={qty} onChange={(e)=>setqty(e.target.value)}>
                            {[...Array(product.countInStock).keys()].map((x)=>(
                                <option key={x+1} value={x+1}>
                                    {x+1}
                                </option>
                            ))}
                        </select>
                    </p>
                    <p>
                        <button type="button" onClick={addtoHandler}>Add to cart</button>
                    </p>
                </div>
            </div>
            </>    
            )}
        </div>
    )
}

export default Productscreen;