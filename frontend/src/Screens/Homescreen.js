import './Homescreen.css';
import Product from '../components/Product';

import {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';

//actions
import { getProducts as listProducts } from "../redux/actions/Productaction";


const Homescreen = () => {

    const dispatch = useDispatch();

    const getProducts = useSelector(state => state.getProducts);
    const {products,loading,error} = getProducts;

    useEffect(() => {
        dispatch(listProducts())
    },[dispatch]);
    return (
        <div className="homescreen">
            <h2 className="homescreen__title">Latest Product</h2>
            <div className="homescreen__products">
            {loading ? (
          <h2>Loading...</h2>
        ) : error ? (
          <h2>{error}</h2>
        ) : (
          products.map((product) => (
            <Product
              key={product._id}
              name={product.name}
              description={product.description}
              price={product.price}
              imageURL={product.imageURL}
              productId={product._id}
            />
          ))
        )}
                
            </div>
        </div>
    );
};

export default Homescreen;