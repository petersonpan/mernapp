import "./App.css";
import { useState } from 'react';
import { BrowserRouter as Router , Routes , Route} from 'react-router-dom';

import Homescreen from './Screens/Homescreen';
import Cartscreen from './Screens/Cartscreen';
import Productscreen from './Screens/Productscreen';

import NavBar from "./components/NavBar";
import BackDrop from "./components/BackDrop";
import SideDrawer from "./components/SideDrawer";

function App() {
  const [sideToggle,setSideToggle] = useState(false);

  return (
    <div className="App">
      <Router>
        <NavBar click={() => setSideToggle(true)} />
        <SideDrawer show={sideToggle} click={() => setSideToggle(false)} />
        <BackDrop show={sideToggle} click={() => setSideToggle(false)}/>
        <main>
          <Routes>
            <Route path="/" element={<Homescreen/>} />
            <Route path="/product/:id" element={<Productscreen/>} />            
            <Route path="/cart" element={<Cartscreen/>} />
          </Routes>
        </main>
      </Router>
    </div>
  );
}

export default App;