const Product = require('../models/products');

const getAllProducts = async(req,res) => {
    try{
        const prd = await Product.find({});
        res.json(prd); 
    }catch(err){
        console.log(err);
        res.status(500).json({message : "server error"});
    }
}

const getProductById = async(req,res) => {
    try{
        const prd = await Product.findById(req.params.id);
        res.json(prd);
    }catch(err){
        console.log(err);
        res.status(500).json({message : "server error"});
    }
}


module.exports = {
    getAllProducts,
    getProductById
} 