const dotenv = require('dotenv').config;
   
const express = require("express");
const productRoutes = require('./routes/productRoutes');
const app = express();
const connectDB = require('./config/db');

app.use(express.json());
app.use("/api/product",productRoutes)

const PORT = process.env.PORT || 5000;
connectDB();

app.listen(PORT,()=> console.log(`Server running on port ${PORT}`));