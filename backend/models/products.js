const mongoose=require("mongoose");

function getNextSequence(name) {
    var ret = db.counters.findAndModify(
           {
             query: { _id: name },
             update: { $inc: { seq: 1 } },
             new: true
           }
    );
  
    return ret.seq;
  }

const ProductSchema = new mongoose.Schema({

        name : {
            type : String,
            required : true
        },
        description : {
            type : String,
            required : true
        },
        price : {
            type : Number,
            required : true
        },
        countInStock : {
            type : Number,
            required : true    
        },
        imageURL : {
             type : String,
             required : true   
        }
})

const Products = mongoose.model("product",ProductSchema);

module.exports = Products;